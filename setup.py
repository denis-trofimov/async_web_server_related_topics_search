#!/usr/bin/env python3
from setuptools import setup, find_packages

setup(name="related_topic", packages=find_packages(), install_requires=["aiohttp>=3.0.0"])
