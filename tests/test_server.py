import pytest
from aiohttp import web
from related_topic.server import app_factory


@pytest.fixture
def client(loop, aiohttp_client):
    app = app_factory()
    return loop.run_until_complete(aiohttp_client(app))


@pytest.mark.parametrize(
    "input, expected",
    [
        ("где купить зимние шины", ["товары"]),
        ("борща любимого рецепт", ["кухня"]),
        ("тайская кухня", ["кухня", "товары"]),
        ("кухня", []),
    ],
)
async def test_handle(client, input, expected):
    params = {"text": input}
    resp = await client.get("/", params=params)
    assert resp.status == 200
    related = await resp.json()
    assert set(related) == set(expected)


async def test_no_text_sent(client):
    resp = await client.get("/")
    assert resp.status == 400
