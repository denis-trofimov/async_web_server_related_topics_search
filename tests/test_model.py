from related_topic.model import Model
import pytest


@pytest.fixture
def model_instance():
    return Model()


@pytest.mark.parametrize(
    "input, expected",
    [
        ("где купить зимние шины", ["товары"]),
        ("борща любимого рецепт", ["кухня"]),
        ("тайская кухня", ["кухня", "товары"]),
        ("кухня", []),
        ("", []),
    ],
)
def test_get_related_topic(model_instance, input, expected):
    related = model_instance.get_related_topic(input)
    assert set(related) == set(expected)
