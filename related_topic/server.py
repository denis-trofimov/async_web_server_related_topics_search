from aiohttp import web
from related_topic.model import Model


async def handle(request):
    text = request.query.get("text")
    if text is None:
        raise web.HTTPBadRequest(text='Shouls pass "text" query parameter!')
    topics = request.app["model"].get_related_topic(text)
    return web.json_response(topics)


def app_factory(argv=None):
    app = web.Application()
    app.add_routes([web.get("/", handle)])
    app["model"] = Model()
    return app


if __name__ == "__main__":
    app = app_factory()
    web.run_app(app)
