"""
- новости: "деревья на Садовом кольце", "добрый автобус", "выставка IT-технологий";
- кухня: "рецепт борща", "яблочный пирог", "тайская кухня";
- товары: "Дети капитана Гранта", "зимние шины", "Тайская кухня";
"""
from typing import Dict


class Model:
    def __init__(
        self,
        topics_initial: Dict = {
            "новости": ["деревья на Садовом кольце", "добрый автобус", "выставка IT-технологий"],
            "кухня": ["рецепт борща", "яблочный пирог", "тайская кухня"],
            "товары": ["Дети капитана Гранта", "зимние шины", "Тайская кухня"],
        },
    ):
        """ Substitute string sentences in topics values by keywords sets."""
        self.topics = {}
        for topic, keywords in topics_initial.items():
            topic_keywords = [frozenset(sentence.lower().split()) for sentence in keywords]
            self.topics[topic] = topic_keywords

    def get_related_topic(self, text: str):
        """ Get related topics for text.
        Правило принадлежности запроса теме:
        1. Если набор слов из запроса содержит в себе все слова какой-либо из фраз,
        то запрос считается соответствующим теме. Иначе - не соответствующим.
        2. Порядок слов в запросе и во фразах не учитывается.

        Примеры:
        Запрос "где купить зимние шины" соответствует теме "товары", т.к. содержит в себе все слова из фразы "зимние шины".
        Запрос "борща любимого рецепт" соответствует теме "кухня", т.к. содержит в себе все слова из фразы "рецепт борща".
        Запрос "тайская кухня" соответствует двум темам: "кухня" и "товары".
        Запрос "кухня" не соответствует ни одной теме, т.к. не включает в себя целиком слова ни одной из фраз.
        """
        words = set(text.lower().split())
        related = []
        for topic, keywords in self.topics.items():
            for keyword_set in keywords:
                if words >= keyword_set:
                    related.append(topic)
                    break
        return related
